package com.emids.hi.common.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class PremiumTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id=0;
	private String partyName="";
	private int partyAge=0;
	private String gender="";
	private String currhealthHyperTension="";
	private String currhealtBloodPresure="";
	private String currhealthBloodSugar="";
	private String currhealthOverWeight="";
	private String habSmoking="";
	private String habAlcohol="";
	private String habDailyExercise="";
	private String habDrugs="";
	private BigDecimal premium=new BigDecimal(0.0);
	
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public int getPartyAge() {
		return partyAge;
	}
	public void setPartyAge(int partyAge) {
		this.partyAge = partyAge;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCurrhealthHyperTension() {
		return currhealthHyperTension;
	}
	public void setCurrhealthHyperTension(String currhealthHyperTension) {
		this.currhealthHyperTension = currhealthHyperTension;
	}
	public String getCurrhealtBloodPresure() {
		return currhealtBloodPresure;
	}
	public void setCurrhealtBloodPresure(String currhealtBloodPresure) {
		this.currhealtBloodPresure = currhealtBloodPresure;
	}
	public String getCurrhealthBloodSugar() {
		return currhealthBloodSugar;
	}
	public void setCurrhealthBloodSugar(String currhealthBloodSugar) {
		this.currhealthBloodSugar = currhealthBloodSugar;
	}
	public String getCurrhealthOverWeight() {
		return currhealthOverWeight;
	}
	public void setCurrhealthOverWeight(String currhealthOverWeight) {
		this.currhealthOverWeight = currhealthOverWeight;
	}
	public String getHabSmoking() {
		return habSmoking;
	}
	public void setHabSmoking(String habSmoking) {
		this.habSmoking = habSmoking;
	}
	public String getHabAlcohol() {
		return habAlcohol;
	}
	public void setHabAlcohol(String habAlcohol) {
		this.habAlcohol = habAlcohol;
	}
	public String getHabDailyExercise() {
		return habDailyExercise;
	}
	public void setHabDailyExercise(String habDailyExercise) {
		this.habDailyExercise = habDailyExercise;
	}
	public String getHabDrugs() {
		return habDrugs;
	}
	public void setHabDrugs(String habDrugs) {
		this.habDrugs = habDrugs;
	}
	public BigDecimal getPremium() {
		return premium;
	}
	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
