package com.emids.hi.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.emids.hi.common.to.PremiumTO;
import com.emids.hi.dao.PremiumDAOimpl;

@RestController
@EnableWebMvc
public class PremiumController {
 
	Logger log=(Logger) Logger.getInstance(PremiumController.class);
	@RequestMapping(path = "/premiumCalculate.htm", method = RequestMethod.POST)
	  public String calculatePremium(@ModelAttribute("premiumTO") PremiumTO primTO, Model map,HttpServletRequest request, HttpServletResponse response) {
		System.out.println("controller");
		primTO = calculatePremium(primTO);	
		map.addAttribute("premiumTO", primTO);
		new PremiumDAOimpl().createPremium(primTO);
		return "premium.jsp";
		   
	  }
	private PremiumTO calculatePremium(PremiumTO prmTO) {
        BigDecimal basePremium=new BigDecimal(5000.00);
		if(prmTO.getPartyAge()>18 && prmTO.getPartyAge()<=40) {
			basePremium=basePremium.add(basePremium.multiply(new BigDecimal(10)).divide(new BigDecimal(100)));
		}
		else if(prmTO.getPartyAge()>40) {
			basePremium=basePremium.add(basePremium.multiply(new BigDecimal(20)).divide(new BigDecimal(100)));
		}
	   if(prmTO.getGender().equals("M")) {
		   basePremium=basePremium.add(basePremium.multiply(new BigDecimal(2)).divide(new BigDecimal(100)));
	   }

	   
	   if(prmTO.getHabSmoking().equals("Yes")) {
		   basePremium=basePremium.add(basePremium.multiply(new BigDecimal(3)).divide(new BigDecimal(100)));
		 }
	   if(prmTO.getHabAlcohol().equals("Yes")) {
		   basePremium=basePremium.add(basePremium.multiply(new BigDecimal(3)).divide(new BigDecimal(100)));
		 }
		   
	   if(prmTO.getHabDrugs().equals("Yes")) {
		   basePremium=basePremium.add(basePremium.multiply(new BigDecimal(3)).divide(new BigDecimal(100)));
		 }
		   
	   if(prmTO.getCurrhealtBloodPresure().equals("Yes")) {
		   basePremium=basePremium.add(basePremium.multiply(new BigDecimal(1)).divide(new BigDecimal(100)));
		 }
	   if(prmTO.getCurrhealthBloodSugar().equals("Yes")) {
		   basePremium=basePremium.add(basePremium.multiply(new BigDecimal(1)).divide(new BigDecimal(100)));
		 }
		   
	   if(prmTO.getCurrhealthHyperTension().equals("Yes")) {
		   basePremium=basePremium.add(basePremium.multiply(new BigDecimal(1)).divide(new BigDecimal(100)));
		 }
	   if(prmTO.getCurrhealthOverWeight().equals("Yes")) {
		   basePremium=basePremium.add(basePremium.multiply(new BigDecimal(1)).divide(new BigDecimal(100))).setScale(2, RoundingMode.HALF_UP);
		 }
		  if(prmTO.getHabDailyExercise().equals("Yes")) {
				 BigDecimal discountPercent = new BigDecimal(3.0);
				 BigDecimal discountAmount = basePremium.multiply(discountPercent).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
				 basePremium = basePremium.subtract(discountAmount);
			   }
	   System.out.println(basePremium);
	   prmTO.setPremium(basePremium); 
		return prmTO;
	}
	
	
	@RequestMapping(path = "/premiumPage.htm", method = RequestMethod.GET)
	  public String calculatePremium(HttpServletRequest request, HttpServletResponse response) {
		   
		return "premium.jsp";
		   
	  }
	
	@RequestMapping(path = "/premiumPagelist.htm", method = RequestMethod.GET)
	  public String getPremiumPage(HttpServletRequest request, HttpServletResponse response) {
		   
		return "premiumlist.jsp";
		   
	  }
	@RequestMapping(path = "/fetchAllPremiums.htm", method = RequestMethod.GET)
	  public String  getPremiums(HttpServletRequest request, HttpServletResponse response) {
		   

		List<PremiumTO>	premiums=new PremiumDAOimpl().getAllPremiumsDetails();
		
		ObjectMapper mapper=new ObjectMapper(); 
	    String jsonString = null;
		try {
			jsonString = mapper.writeValueAsString(premiums);
			response.getWriter().write(jsonString);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
		   
	  }
	
	/*public static void main(String []args) {
		BigDecimal basePremium=new BigDecimal(5000.00);
	
		basePremium=basePremium.add(basePremium.multiply(new BigDecimal(10)).divide(new BigDecimal(100)));
	
			System.out.println(basePremium);
		
			
			PremiumTO prmTO=new PremiumTO();
			prmTO.setGender("M");
			prmTO.setHabAlcohol("Yes");
			prmTO.setHabDailyExercise("Yes");
			prmTO.setPartyAge(34);
			prmTO.setCurrhealthOverWeight("Yes");
			//calculatePremium(prmTO);
		
	}*/
}
