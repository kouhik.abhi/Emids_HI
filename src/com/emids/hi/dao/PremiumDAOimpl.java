package com.emids.hi.dao;

import java.util.List;

import com.emids.hi.common.to.PremiumTO;
import com.emids.hi.dao.rowmappers.PremiumRowMapper;
import com.emids.hi.utils.DAOInitializer;

public class PremiumDAOimpl extends DAOInitializer{

	private static  PremiumDAOimpl premiumDAOimpl=new PremiumDAOimpl();
	public List<PremiumTO> getAllPremiumsDetails() {
		
		String sql ="select * from premium";		
		return getJdbcTemplate().query(sql, new PremiumRowMapper());
	}

	public int createPremium(PremiumTO premTO) {
		String sql ="insert into  premium values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		return getJdbcTemplate().update(sql,premTO.getId(),premTO.getPremium(),premTO.getPartyName(),premTO.getPartyAge(),premTO.getHabSmoking(),
			premTO.getHabDrugs(),premTO.getHabDailyExercise(),premTO.getHabAlcohol(),premTO.getGender(),premTO.getCurrhealthOverWeight(),
			premTO.getCurrhealthHyperTension(),premTO.getCurrhealthBloodSugar(),premTO.getCurrhealtBloodPresure());
	}

    public static PremiumDAOimpl getInstance() {
    	if(premiumDAOimpl!=null) {
    		premiumDAOimpl = new PremiumDAOimpl();
    	}
    	return premiumDAOimpl;
    }
	
}
