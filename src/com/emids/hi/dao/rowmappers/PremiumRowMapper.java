package com.emids.hi.dao.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.emids.hi.common.to.PremiumTO;

public class PremiumRowMapper implements ResultSetExtractor<List<PremiumTO>> {

	public List<PremiumTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
		List<PremiumTO> details=new ArrayList<PremiumTO>();
		while(rs.next()) {
			PremiumTO premiumTO=new PremiumTO();
			premiumTO.setCurrhealtBloodPresure(rs.getString("currhealtbloodPresure"));
			premiumTO.setCurrhealthBloodSugar(rs.getString("currhealthBloodSugar"));
			premiumTO.setCurrhealthHyperTension(rs.getString("currhealthHyperTension"));
			premiumTO.setCurrhealthOverWeight(rs.getString("currhealthOverWeight"));
			premiumTO.setGender(rs.getString("gender"));
			premiumTO.setHabAlcohol(rs.getString("Alcohol"));
			premiumTO.setHabDailyExercise(rs.getString("DailyExercise"));
			premiumTO.setPartyAge(rs.getInt("partyAge"));
			premiumTO.setPartyName(rs.getString("partyName"));
			premiumTO.setPremium(rs.getBigDecimal("premium"));
			
			details.add(premiumTO);
		}
		
		return details;
	}

}
