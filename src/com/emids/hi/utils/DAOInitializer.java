package com.emids.hi.utils;


import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

public class DAOInitializer {

    private static DAOInitializer daoInit;
    private static DataSource dataSource;
    private static JdbcTemplate template = null;
    protected static ApplicationContext actx;
    
    static {
    	try{
	actx = new ClassPathXmlApplicationContext(
		"com/emids/hi/utils/SpringDataBaseConfig.xml");
    }
    catch(Exception e){
    	e.printStackTrace();
    }
    }
    public DAOInitializer() {

    }

   /* public synchronized DAOInitializer getInstance() {
	if (daoInit == null) {
	    synchronized (DAOInitializer.class) {
		daoInit = new DAOInitializer();
	    }
	}
	return daoInit;

    }
*/
    @SuppressWarnings("static-access")
    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
	template = new JdbcTemplate(dataSource);
    }

    public static DataSource getDataSource() {
	return dataSource;
    }

    public static JdbcTemplate getJdbcTemplate() {
	return template;
    }
}
