<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Calculated Premiums</title>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/angular.min.js"></script>
 <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"></link>
<script type="text/javascript">
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
    $http.get("http://localhost:8080/EmidsHI/fetchAllPremiums.htm")
    .then(function(response) {
        $scope.premiums = response.data;
    });
});

</script>
</head>
<body ng-app="myApp">
<div class="container">
<table class="table table-hover" ng-controller="myCtrl" >
<thead><tr>
			<th>Party Name</th>
			<th >Age</th>
			<th >Gender</th>
			<th>Premium</th>
			<th >Over Weight</th>
			<th>Blood Pressure</th>
			<th>Hyper Tension</th>
			<th>Alcohol</th>
			</tr></thead>
     <tbody>
             <tr ng-repeat="premium in premiums">
                              <td>{{premium.partyName}}</td>
                               <td>{{premium.partyAge}}</td>
                                <td>{{premium.gender}}</td>
                                <td>{{premium.premium}}</td>
                                <td>{{premium.currhealthOverWeight}}</td>
                                <td>{{premium.currhealtBloodPresure}}</td>
                                <td>{{premium.currhealthHyperTension}}</td>
                                 <td>{{premium.habAlcohol}}</td>
                                
                                
 
 </table>  
  <a href="premiumPage.htm" class="btn btn-primary">Create Premium</a>
</div>   
</body>
</html>