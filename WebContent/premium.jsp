    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Premium Calculator</title>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/angular.min.js"></script>
 <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"></link>
<script type="text/javascript">
$(function(){
	
	 if('${premiumTO.premium}'>0){
			
			$("#premium").show();	
		} 
	
	 if('${premiumTO.currhealthHyperTension}'=='Yes'){
		
		$("#hyper").prop("checked",true);	
	} 
	 if('${premiumTO.currhealtBloodPresure}'=='Yes'){
			
			$("#bloodpresure").prop("checked",true);	
		} 
	 if('${premiumTO.currhealthBloodSugar}'=='Yes'){
			
			$("#bloodsugar").prop("checked",true);	
		} 
		 if('${premiumTO.currhealthOverWeight}'=='Yes'){
				
				$("#overweight").prop("checked",true);	
			} 
		 if('${premiumTO.habSmoking}'=='Yes'){
				
				$("#smookingr").prop("checked",true);	
			} 
		 if('${premiumTO.habAlcohol}'=='Yes'){
				
				$("#alcohol").prop("checked",true);	
			} 
		 if('${premiumTO.habDailyExercise}'=='Yes'){
				
				$("#dailyexercise").prop("checked",true);	
			} 
		 if('${premiumTO.habDrugs}'=='Yes'){
				
				$("#drugs").prop("checked",true);	
			} 
	 
	 $("#gender").val('${premiumTO.gender}');
});

</script>
 <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"></link>
</head>
<body>
<div style="margin-top:40px">
 <form:form class="form-horizontal" modelAttribute="premiumTO" method="post" action="premiumCalculate.htm">
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">Name:</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" value="${premiumTO.partyName}" name="partyName" placeholder="Enter Name">
    </div>
 </div>
 <div class="form-group">
    <label class="control-label col-sm-2" for="email">Gender:</label>
    <div class="col-sm-3">
      <select  class="form-control" id="gender" name="gender" >
      <option value="">--select gender</option>
      <option value="M">Male </option>
          <option value="F">Female </option>
          <option value="O">Others </option>
      </select>
    </div>
 </div>
 <div class="form-group">
    <label class="control-label col-sm-2" for="email">Age:</label>
    <div class="col-sm-3">
       <input type="number" class="form-control"  value="${premiumTO.partyAge}" name="partyAge" placeholder="Enter Age">
    </div>
 </div>
   <div class="form-group">
    <label class="control-label col-sm-2">Current Health:</label>
    <div class="col-sm-10">
     <div class="form-check">
       <label class="form-check-label"><input type="checkbox" value="Yes" id="hyper" class="form-check-input" name="currhealthHyperTension">
       Hyper Tension</label>
    </div>
    <div class="form-check">
       <label class="form-check-label"><input type="checkbox" id="bloodpressure" class="form-check-input" value="Yes" name="currhealtBloodPresure">
       Blood Pressure</label>
    </div>
    <div class="form-check">
       <label class="form-check-label"><input type="checkbox" id="bloodsugar" class="form-check-input" value="Yes" name="currhealthBloodSugar">
        Blood Sugar</label>
    </div>
    <div class="form-check">
       <label class="form-check-label"><input type="checkbox" id="healthoverweight" class="form-check-input" value="Yes" name="currhealthOverWeight">
         Over Weight</label>
    </div>
   </div> 
    </div>
 <div class="form-group">
    <label class="control-label col-sm-2" for="email">Habits:</label>
    <div class="col-sm-10">
         <div class="form-check">
       <label class="form-check-label"><input type="checkbox" id="smoking" value="Yes" class="form-check-input" name="habSmoking">
       Smoking</label>
 </div>
      <div class="form-check">
       <label class="form-check-label"><input type="checkbox" id="alcohol" value="Yes" class="form-check-input" name="habAlcohol">
      Alcohol</label>
    </div>
    <div class="form-check">
       <label class="form-check-label"><input type="checkbox" id="dailyexercise" value="Yes" class="form-check-input" name="habDailyExercise">
      Daily Exercise</label>
    </div>
    <div class="form-check">
       <label class="form-check-label"><input type="checkbox" id="drugs" value="Yes" class="form-check-input" name="habDrugs">
       Drugs</label>
    </div>
      <input type="submit" class="btn btn-primary" value="Calculate Premium"/>
      
      <a href="premiumPagelist.htm" class="btn btn-primary">View All Premiums</a>
  </div>

 </div>
</form:form> 
</div>
<div >
<label style="margin-left:100px;display:none" id="premium">Health Insurance Premium for ${premiumTO.partyName} : Rs.  ${premiumTO.premium}</label><label></label>
</div>
</body>
</html>