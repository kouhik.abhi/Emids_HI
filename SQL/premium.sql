-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2017 at 07:49 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emids`
--

-- --------------------------------------------------------

--
-- Table structure for table `premium`
--

CREATE TABLE `premium` (
  `id` int(255) NOT NULL,
  `premium` decimal(37,13) NOT NULL DEFAULT '0.0000000000000',
  `partyname` varchar(255) DEFAULT NULL,
  `partyage` int(255) NOT NULL DEFAULT '0',
  `habsmoking` varchar(255) DEFAULT NULL,
  `habdrugs` varchar(255) DEFAULT NULL,
  `DailyExercise` varchar(255) DEFAULT NULL,
  `alcohol` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `currhealthOverWeight` varchar(255) DEFAULT NULL,
  `CurrhealthHyperTension` varchar(255) DEFAULT NULL,
  `CurrhealthBloodSugar` varchar(255) DEFAULT NULL,
  `CurrhealtBloodPresure` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `premium`
--

INSERT INTO `premium` (`id`, `premium`, `partyname`, `partyage`, `habsmoking`, `habdrugs`, `DailyExercise`, `alcohol`, `gender`, `currhealthOverWeight`, `CurrhealthHyperTension`, `CurrhealthBloodSugar`, `CurrhealtBloodPresure`) VALUES
(1, '5889.1371449000000', 'koushik', 24, 'Yes', 'Yes', 'Yes', '', 'M', '', 'Yes', 'Yes', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `premium`
--
ALTER TABLE `premium`
  ADD KEY `prm_id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `premium`
--
ALTER TABLE `premium`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
